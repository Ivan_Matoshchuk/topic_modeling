from binascii import Error
import logging
import os
import sys
import time
import pickle
from pathlib import Path
from typing import List, Union, Tuple

import nltk
import pandas as pd
import spacy
from joblib import Parallel, delayed
from nltk.stem.wordnet import WordNetLemmatizer
from pattern.en import parse
from tqdm.notebook import tqdm


nlp = spacy.load("en_core_web_sm")

projectpath = Path(__file__).parent.parent.parent
path_to_preprocessed_text = os.path.join(projectpath, "preprocessed_text")

logging.basicConfig(format="%(name)s:%(levelname)s:%(message)s")
log = logging.getLogger(name=os.path.basename(__file__))
log.setLevel(logging.INFO)


class TextPreprocessor(object):

    """
    Class for preprocessing technician remarks.

    tagger: [pattern, nltk, spacy] different tagging models, is a hyperparameter.
    only_these_pos: which part-of-speaches to use. Either tuple of POS tags or None.
    lemmatize: True/False, whether we would like to lemmatize a word
    """

    def __init__(
        self,
        tagger: str = "pattern",
        only_these_pos: Union[Tuple[str], None] = ("V", "N", "J", "R"),
        lemmatize: bool = True,
    ):

        self.tagger = tagger
        
        if only_these_pos is None:
            self.only_these_pos = ""
        else:
            self.only_these_pos = only_these_pos

        self.lemmatize = lemmatize

    def get_lemma(self, word):
        return WordNetLemmatizer().lemmatize(word)

    def preprocess_text_nltk(self, sent: str):
        tokens = nltk.word_tokenize(sent.lower())
        tokens = [t for t in tokens if t not in nlp.Defaults.stop_words and len(t) > 3]
        if self.lemmatize:
            tokens = [self.get_lemma(t) for t in tokens]
        tokens = [i[0] for i in nltk.pos_tag(tokens) if i[1].startswith(self.only_these_pos)]

        return tokens

    def normalize(self, text: List[str]):

        tokens = [t for t in text if t not in nlp.Defaults.stop_words and len(t) > 3]
        if self.lemmatize:
            tokens = [self.get_lemma(t) for t in tokens]

        return tokens

    def preprocess_text(
        self,
        text: pd.Series,
        save_to_file_name: Union[str, None] = None,
    ):

        """
        text: raw text that should be preprocessed
        save_to_file_name: name of the file where we should save the preprocessed text


        """
        start = time.time()

        tqdm.pandas()

        if (save_to_file_name is not None) and (save_to_file_name in os.listdir(path_to_preprocessed_text)):
            log.error(f"{save_to_file_name} already exists!")
            raise FileExistsError

        if self.tagger == "pattern":

            """
            There is a bug with pattern package, it can be fixed by starting it several times...
            TODO: fix source code, error with "yield"
            """

            try:

                text_verbs_nouns = text.apply(
                    lambda x: [i[0].lower() for i in sum(parse(x).split(), []) if i[1].startswith(self.only_these_pos)]
                )
            except RuntimeError:
                try:
                    text_verbs_nouns = text.apply(
                        lambda x: [i[0].lower() for i in sum(parse(x).split(), []) if i[1].startswith(self.only_these_pos)]
                    )
                except RuntimeError:
                    try:
                        text_verbs_nouns = text.apply(
                            lambda x: [i[0].lower() for i in sum(parse(x).split(), []) if i[1].startswith(self.only_these_pos)]
                        )
                    except RuntimeError:
                        text_verbs_nouns = text.apply(
                            lambda x: [i[0].lower() for i in sum(parse(x).split(), []) if i[1].startswith(self.only_these_pos)]
                        )

            text_verbs_nouns = Parallel(n_jobs=-1)(
                delayed(self.normalize)(sent) for sent in tqdm(text_verbs_nouns, desc="preprocessing with pattern")
            )

        elif self.tagger == "spacy":

            text_verbs_nouns = text.apply(
                lambda x: [
                    i.lemma_ for i in nlp(x) if i.tag_.startswith(self.only_these_pos) and i.dep_ != "aux" and i.is_stop != True
                ]
            )

        elif self.tagger == "nltk":
            text_verbs_nouns = [self.preprocess_text_nltk(sent) for sent in tqdm(text, desc="preprocessing with nltk")]
     

        if os.path.join(path_to_preprocessed_text, save_to_file_name) is not None:

            with open(os.path.join(path_to_preprocessed_text, save_to_file_name), "wb") as f:
                pickle.dump(text_verbs_nouns, f)

        log.info(f"Finished! Time taken: {round(time.time() - start, 3)} sec.")

        return text_verbs_nouns
