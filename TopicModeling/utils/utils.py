from typing import List
import nltk
from nltk.corpus import wordnet

from IPython.display import display
from tqdm import tqdm_notebook as tqdm

from joblib import Parallel, delayed
from multiprocessing import Pool

import pandas as pd
import pickle






class GetVerbsNouns:
    """
    Get Nouns, Verbs, Noun and Verb phrases from text
    """

    # Rule for NP chunk and VB Chunk
    grammar = r"""
        NBAR:
            {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns
            {<RB.?>*<VB.?>*<JJ>*<VB.?>+<VB>?} # Verbs and Verb Phrases
            
        NP:
            {<NBAR>}
            {<NBAR><IN><NBAR>}  # Above, connected with in/of/etc...
            
    """

    # lemmatizer
    lemmatizer = nltk.WordNetLemmatizer()
    # Chunking
    cp = nltk.RegexpParser(grammar)

    def __init__(self, documents: List[str], n_jobs):

        self.documents = documents
        self.n_jobs = n_jobs

    def leaves(self, tree):
        """Finds NP (nounphrase) leaf nodes of a chunk tree."""
        for subtree in tree.subtrees(filter=lambda t: t.label() == "NP"):
            yield subtree.leaves()

    def get_word_postag(self, word):
        if nltk.pos_tag([word])[0][1].startswith("J"):
            return wordnet.ADJ
        if nltk.pos_tag([word])[0][1].startswith("V"):
            return wordnet.VERB
        if nltk.pos_tag([word])[0][1].startswith("N"):
            return wordnet.NOUN
        else:
            return wordnet.NOUN

    def normalise(self, word):
        """Normalises words to lowercase and stems and lemmatizes it."""
        word = word.lower()
        postag = self.get_word_postag(word)
        word = self.lemmatizer.lemmatize(word, postag)
        return word

    def get_terms(self, tree):
        for leaf in self.leaves(tree):
            terms = [self.normalise(w) for w, t in leaf]
            yield terms

    def generate_postag(self):

        # tokens = [nltk.word_tokenize(sent) for sent in tqdm(self.documents, desc = "tokenizing")]
        # postag = [nltk.pos_tag(sent) for sent in tqdm(tokens, desc = "pos-tagging")]

        tokens = Parallel(n_jobs=self.n_jobs)(
            delayed(nltk.word_tokenize)(sent) for sent in tqdm(self.documents, desc="tokenizing")
        )
        postag = Parallel(n_jobs=self.n_jobs)(delayed(nltk.pos_tag)(sent) for sent in tqdm(tokens, desc="pos-tagging"))

        return postag

    def get_features_per_doc(self, doc):
        tree = self.cp.parse(doc)
        terms = self.get_terms(tree)

        features = []
        for term in terms:
            _term = ""
            for word in term:
                _term += " " + word
            features.append(_term.strip())

        return features

    def _generate_VerbNounPhrases(self):

        # postag = self.generate_postag()
        try:
            postag = self.postag
        except AttributeError:
            postag = self.generate_postag()
            self.postag = postag

        # pool = Pool(processes=8)

        with Pool(4) as p:
            results = list(tqdm(p.imap(self.get_features_per_doc, postag), total=len(postag)))

        # results = pool.map(self.get_features_per_doc, postag)

        # results = Parallel(n_jobs=self.n_jobs)(
        #     delayed(self.get_features_per_doc)(doc) for doc in tqdm(postag, desc="getting-VerbNoun-Phrases")
        # )
        return results

    def generate_VerbNounPhrases(self):
        try:
            postag = self.postag
        except AttributeError:
            postag = self.generate_postag()
            self.postag = postag

        final = []
        for doc in tqdm(postag, desc="getting-VerbNoun-Phrases"):

            features = []
            tree = self.cp.parse(doc)
            terms = self.get_terms(tree)

            features = []
            for term in terms:
                _term = ""
                for word in term:
                    _term += " " + word
                features.append(_term.strip())

            final.append(features)

        return final

    def generate_Verb_Nouns(self):

        return 1