import os
from pathlib import Path
import logging

from typing import List, Union
import pandas as pd
import gensim
from gensim.models.ldamodel import LdaModel
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
from gensim import corpora
from gensim.models.coherencemodel import CoherenceModel
from tqdm.notebook import tqdm
from datetime import datetime
from joblib import Parallel, delayed

logging.basicConfig(format="%(name)s:%(levelname)s:%(message)s")
log = logging.getLogger(name=os.path.basename(__file__))
log.setLevel(logging.INFO)

projectpath = Path(__file__).parent.parent.parent
path_to_output = os.path.join(projectpath, "output")


class LDATopics(object):
    """
    class that uses LDA technique for generating topics from input documents.
    LDA : Each document can be described by a distribution of topics and each topic can be described by a distribution of words
    """

    def __init__(
        self, text: List[List[str]], no_below: int = 50, no_above: float = 0.10, gram: Union[str, None] = None
    ):

        """
        text : Preprocessed List of sentences , where each sentence is represented as a list of tokens.

        in dictionary-creation:
            no_below : Keep tokens which are contained in at least `no_below` documents.
            no_above : Keep tokens which are contained in no more than `no_above` documents  (fraction of total corpus size, not an absolute number).

        gram : specify between "bigram", "trigram" or None.
        """

        self.no_below = no_below
        self.no_above = no_above

        if gram == "bigram":
            bigram = gensim.models.Phrases(text, min_count=5, threshold=20)  # higher threshold fewer phrases.
            bigram_mod = gensim.models.phrases.Phraser(bigram)

            self.text = [bigram_mod[i] for i in text]

        elif gram == "trigram":
            bigram = gensim.models.Phrases(text, min_count=5, threshold=20)  # higher threshold fewer phrases.
            trigram = gensim.models.Phrases(bigram[text], threshold=25)
            trigram_mod = gensim.models.phrases.Phraser(trigram)

            self.text = [trigram_mod[i] for i in text]
        elif gram is None:
            self.text = text
        else:
            log.error('possible values for argument "gram" are unigram, bigram or None!\nUsing None')
            self.text = text

    def create_dictionary(self):

        dictionary = corpora.Dictionary(self.text)
        dictionary.filter_extremes(no_below=self.no_below, no_above=self.no_above)

        return dictionary

    def generate_topics(
        self,
        output_html_name: Union[str, None] = None,
        min_topics: int = 2,
        max_topics: int = 20,
        step: int = 2,
        coherence_measure: str = "c_uci",
    ):

        """
        output_html_name : name of the html file with PyLDAVis visualization of topics.

        min_topics, max_topics, step : used to iteratively find the best number of topics for the given dataset based on Coherence

        Topic Coherence measures score a single topic by measuring the degree of semantic similarity between high scoring words in the topic.
        """

        self.dictionary = self.create_dictionary()

        # bag of words
        self.corpus = [self.dictionary.doc2bow(doc) for doc in self.text]

        num_topics = list(range(min_topics, max_topics + 1, step))
        coherence_scores = []
        for n in tqdm(num_topics):

            lda = gensim.models.LdaModel(self.corpus, num_topics=n, random_state=123)
            cm = CoherenceModel(model=lda, texts=self.text, dictionary=self.dictionary, coherence=coherence_measure)
            coherence_scores.append(round(cm.get_coherence(), 5))

        # get best number of topics
        scores = list(zip(num_topics, coherence_scores))
        best_num_topics = sorted(scores, key=lambda x: x[1], reverse=True)[0][0]
        log.info(f"\nBest model coherence: {sorted(scores, key=lambda x: x[1], reverse=True)[0][1]}\n")

        # create lda model
        self.lda_model = gensim.models.LdaModel(
            self.corpus, num_topics=best_num_topics, id2word=self.dictionary, per_word_topics=True, random_state=123
        )

        # save interactive html
        if output_html_name is not None:

            vis = gensimvis.prepare(self.lda_model, self.corpus, self.dictionary)

            path_to_vis = os.path.join(
                path_to_output,
                f'{output_html_name}_top_{best_num_topics}_topics_{datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")}.html',
            )
            pyLDAvis.save_html(vis, path_to_vis)

            log.info("pyLDAvis saved!\n")

        return scores, self.lda_model, num_topics

    def create_results_dataframe(self, best_lda_model: LdaModel):
        """
        creates a dataframe with organized results

        topic  |  topic contribution (probability)  |  topic keywords  |  document ( technician remark )
        """

        top2words_mapping = {
            top: [i[0] for i in best_lda_model.show_topic(top)] for top in range(0, best_lda_model.num_topics, 1)
        }

        def get_top_topic(i):

            topics = best_lda_model[i][0]
            top_topic = sorted(topics, key=lambda x: (x[1]), reverse=True)[0]
            return top_topic

        results = Parallel(n_jobs=4)(delayed(get_top_topic)(i) for i in tqdm(self.corpus))

        topics, topic_perc_contribution = zip(*results)

        df_topic = {
            "top_topic": topics,
            "topic_perc_contribution": topic_perc_contribution,
            "topic_keywords": topics,
            "text": self.text,
        }
        df_topic = pd.DataFrame.from_dict(df_topic)
        df_topic["topic_keywords"] = df_topic["topic_keywords"].map(top2words_mapping)

        log.info("created Dataframe!")

        return df_topic
